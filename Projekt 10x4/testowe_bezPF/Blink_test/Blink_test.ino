#include <Wire.h>
//#include <PCF8574.h>
#include <DS3231.h>

//PCF8574 expander;

#include <Timers.h>
Timer dateDisplayTimer;
Timer timerForChangeDisplay;
Timer timerForAlarm;

Timer alarmTimeLong;
bool isAlarmON = false;

bool dateInterval;
bool alarmInterval;

DS3231 clock;
RTCDateTime dt;
RTCAlarmTime alarm;

// v1
//#define lampa_1 13
//#define lampa_2 14
//#define lampa_3 15
//#define lampa_4 16
//#define sekundnik 17
//
//#define cyfra_9 12
//#define cyfra_8 11
//#define cyfra_7 10
//#define cyfra_6 9
//#define cyfra_5 8
//#define cyfra_4 7
//#define cyfra_3 6
//#define cyfra_2 5
//#define cyfra_1 4
//#define cyfra_0 3

//v2
#define lampa_1 13
#define lampa_2 14
#define lampa_3 15
#define lampa_4 16
#define sekundnik 17


#define cyfra_9 4
#define cyfra_8 5
#define cyfra_7 6
#define cyfra_6 7
#define cyfra_5 8
#define cyfra_4 9
#define cyfra_3 10
#define cyfra_2 11
#define cyfra_1 12
#define cyfra_0 3

#define button_1 7    // set Time/Temp/PoisonPrev
#define button_2 6    // set alarm
#define button_3 5    // first value change
#define button_4 4    // second value change

const int buzzer = 2; //buzzer to arduino pin D2

// Variables will change:
int index = 1;
int currentView = 0;

String poisonIndicator = "0001";

int timer = 4;

int displayAnods[] = {
    13 // 1
    ,
    14 // 2
    ,
    15 // 3
    ,
    16 // 4
};

int displayDigits[] = {
    3 // 0
    ,
    4 // 1
    ,
    5 // 2
    ,
    6 // 3
    ,
    7 // 4
    ,
    8 // 5
    ,
    9 // 6
    ,
    10 // 7
    ,
    11 // 8
    ,
    12 // 9
};

byte year, month, date, DoW, hour, minute, second;

// the setup function runs once when you press reset or power the board
void setup()
{
    //Serial.begin(9600);
    //Serial.println("Initialize DS3231");

    dateDisplayTimer.begin(SECS(4));
    timerForChangeDisplay.begin(200);
    timerForAlarm.begin(500);

    alarmTimeLong.begin(SECS(30));
   
    clock.begin();

    //clock.setAlarm2(0, 23, 36,     DS3231_MATCH_M);
    
    // Ustawiamy date i czas z kompilacji szkicu
    //clock.setDateTime(__DATE__, __TIME__);

    // Lub recznie (YYYY, MM, DD, HH, II, SS
    clock.setDateTime(2014, 4, 13, 9, 2, 00);

    // initialize digital pin LED_BUILTIN as an output.
    pinMode(lampa_1, OUTPUT);
    pinMode(lampa_2, OUTPUT);
    pinMode(lampa_3, OUTPUT);
    pinMode(lampa_4, OUTPUT);
    pinMode(sekundnik, OUTPUT);
    
    pinMode(buzzer, OUTPUT); // Set buzzer - pin D2 as an output

    for (int thisPin = 3; thisPin < 12; thisPin++) {
        pinMode(thisPin, OUTPUT);
    }

    //expander_buttons

}

void anodes_OFF()
{
    digitalWrite(lampa_1, LOW);
    digitalWrite(lampa_2, LOW);
    digitalWrite(lampa_3, LOW);
    digitalWrite(lampa_4, LOW);
}

void anodes_ON()
{
    digitalWrite(lampa_1, HIGH);
    digitalWrite(lampa_2, HIGH);
    digitalWrite(lampa_3, HIGH);
    digitalWrite(lampa_4, HIGH);
}

void poisoningPreventionSettings(){
  
   String value = poisonIndicator;
    char charBuf[50];
    value.toCharArray(charBuf, 50);
    for (int i = 3; i <= 3; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(4);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }   
  }

void poisoningPrevention(){

  for (int i = 0; i <= 3; i++) {
    digitalWrite(displayAnods[i], HIGH);

      for (int j = 0; j <= 9; j++) {
          digitalWrite(displayDigits[j], HIGH);
          delay(100);
          digitalWrite(displayDigits[j], LOW);
        }
    //digitalWrite(displayAnods[i], LOW);
    }

    for (int i = 3; i >= 0; i--) {
    //digitalWrite(displayAnods[i], LOW);

      for (int j = 9; j >= 0; j--) {
          digitalWrite(displayDigits[j], HIGH);
          delay(100);
          digitalWrite(displayDigits[j], LOW);
        }
    digitalWrite(displayAnods[i], LOW);
    }  
  }


void displayTime(int timer)
{

    dt = clock.getDateTime();
    String _time = clock.dateFormat("Hi", dt);

    char charBuf[50];
    _time.toCharArray(charBuf, 50);
    for (int i = 0; i <= 3; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(timer);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }

    blink_ON();
}

void displayAlarm(int timer)
{
    alarm = clock.getAlarm2();
    String _alarm = clock.dateFormat("Hi", alarm);

    char charBuf[50];
    _alarm.toCharArray(charBuf, 50);
    for (int i = 0; i <= 3; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(timer);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }

    blink_OFF();
}

void blink_ON()
{
    int _seconds = dt.second;

    if (_seconds % 2 == 0) {
        digitalWrite(sekundnik, HIGH);
    }
    else {
        digitalWrite(sekundnik, LOW);
    }
}

void blink_OFF()
{
  digitalWrite(sekundnik, LOW);
}

void displayTemp(int timer)
{
    String _temp = String(int(round(clock.readTemperature())));
    char charBuf[50];
    _temp.toCharArray(charBuf, 50);
    for (int i = 0; i <= 1; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(4);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }
}

void displayDate(int timer)
{

    dt = clock.getDateTime();
    String _date;

    if (dateDisplayTimer.available()) {
        dateInterval ^= true;
        dateDisplayTimer.restart();
    }

    if (dateInterval) {
        _date = clock.dateFormat("dm", dt);
    }
    else {
        _date = clock.dateFormat("Y", dt);
    }

    char charBuf[50];
    _date.toCharArray(charBuf, 50);
    for (int i = 0; i <= 3; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(4);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }
}

void changeView()
{
    index++;
    index = index % 3;
    //index = index % 2;
    delay(200);
    currentView = index;
}

void changeMinuts()
{

    if (timerForChangeDisplay.available()) {
        minute = dt.minute;
        minute++;
        if (minute == 60) {
            minute = 0;
        }
        clock.setDateTime(dt.year, dt.month, dt.day, dt.hour, minute, 00);
        timerForChangeDisplay.restart();
    }
}

void changeHours()
{

    if (timerForChangeDisplay.available()) {
        hour = dt.hour;
        hour++;
        if (hour == 24) {
            hour = 0;
        }
        clock.setDateTime(dt.year, dt.month, dt.day, hour, dt.minute, 00);

        timerForChangeDisplay.restart();
    }
}

void changeAlarmHours(){
  
  if (timerForChangeDisplay.available()) {
    
        hour = alarm.hour;
        hour++;
        if (hour == 24) {
            hour = 0;
        }        
        clock.setAlarm2(0, hour, alarm.minute,     DS3231_MATCH_M);
        timerForChangeDisplay.restart();
    }
}

void changeAlarmMinuts(){
  
  if (timerForChangeDisplay.available()) {
    
       minute = alarm.minute;
        minute++;
        if (minute == 60) {
            minute = 0;
        }        
        clock.setAlarm2(0, alarm.hour, minute,     DS3231_MATCH_M);
        timerForChangeDisplay.restart();
    }
}

void runAlarm(){

  if (timerForAlarm.available()) {
        alarmInterval ^= true;
        timerForAlarm.restart();
    }
    if (alarmInterval) {
      tone(buzzer, 1500); // Send 1KHz sound signal...

            Serial.println("ALARM ON");
           }
    else {
      noTone(buzzer);     // Stop sound...
      Serial.println("ALARM OFF");
    }    
  }



void loop()
{

    switch (currentView) {
    case 0:        
        displayTime(timer);
               break;
    case 1:
        //Serial.println("Temp on screen");
        displayTemp(timer);
        blink_OFF();
        break;
    case 2:
        poisoningPreventionSettings();
               
        
            //Serial.println("Date on screen");
            //displayDate(timer);
        blink_OFF();        
        break;
    case 3:
        displayAlarm(timer);
            
        break;
    }
       
    

if(poisonIndicator == "0001"){
    if((dt.minute == dt.hour) && (dt.hour == dt.second)){
      poisoningPrevention();
      currentView = 0;
    } 
    }
    
    
  // Sprawdzamy alaram 1
  if (clock.isAlarm2())
  { 
    //Serial.println("ALARM 2 TRIGGERED!");
    alarmTimeLong.restart();     
    isAlarmON = true;
  }
  
  
 }
