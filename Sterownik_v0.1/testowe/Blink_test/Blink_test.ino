#include <Wire.h>
//#include <PCF8574.h>
//
//
//PCF8574 expander;
//#define PCF_Address 0x20 // 0x38 - PCF8574A  or 0x20 - PCF8574 
//

#define lampa_1 19
#define lampa_2 17
#define lampa_3 16//8
#define lampa_4 15
#define lampa_5 14
#define lampa_6 13
#define sekundnik 2

#define cyfra_9 0
#define cyfra_8 2
#define cyfra_7 4
#define cyfra_6 5
#define cyfra_5 6
#define cyfra_4 7
#define cyfra_3 4
#define cyfra_2 9
#define cyfra_1 10
#define cyfra_0 1

#define button_1 7    // set Time/Temp/PoisonPrev
#define button_2 6    // set alarm
#define button_3 5    // first value change
#define button_4 4    // second value change

const int buzzer = 2; //buzzer to arduino pin D2

// Variables will change:
int index = 1;
int currentView = 0;

String poisonIndicator = "0001";

int timer = 4;

int displayAnods[] = {
    13 // 1
    ,
    14 // 2
    ,
    15 // 3
    ,
    16 // 4
};

int displayDigits[] = {
    3 // 0
    ,
    4 // 1
    ,
    5 // 2
    ,
    6 // 3
    ,
    7 // 4
    ,
    8 // 5
    ,
    9 // 6
    ,
    10 // 7
    ,
    11 // 8
    ,
    12 // 9
};

byte year, month, date, DoW, hour, minute, second;

// the setup function runs once when you press reset or power the board
void setup()
{
    pinMode(lampa_1, OUTPUT);
    pinMode(lampa_2, OUTPUT);
    pinMode(lampa_3, OUTPUT);
    pinMode(lampa_4, OUTPUT);
    pinMode(lampa_5, OUTPUT);
    pinMode(lampa_6, OUTPUT);

    digitalWrite(lampa_1, LOW);
    digitalWrite(lampa_2, LOW);
    digitalWrite(lampa_3, LOW);
    digitalWrite(lampa_4, LOW);
    digitalWrite(lampa_5, LOW);
    digitalWrite(lampa_6, LOW);


    
      
    for (int thisPin = 0; thisPin < 10; thisPin++) {
        pinMode(thisPin, OUTPUT);
        digitalWrite(thisPin, LOW);        
    }


      


//    expander.begin(PCF_Address);    
//    expander.pinMode(4, OUTPUT);
//    expander.pinMode(5, OUTPUT);
//    expander.digitalWrite(4, HIGH);  //t6
//    expander.digitalWrite(5, LOW);  //sek
   
    
}

void anodes_OFF()
{
    digitalWrite(lampa_1, LOW);
    digitalWrite(lampa_2, LOW);
    digitalWrite(lampa_3, LOW);
    digitalWrite(lampa_4, LOW);
}

void anodes_ON()
{
    digitalWrite(lampa_1, HIGH);
    digitalWrite(lampa_2, HIGH);
    digitalWrite(lampa_3, HIGH);
    digitalWrite(lampa_4, HIGH);
}

void poisoningPreventionSettings(){
  
   String value = poisonIndicator;
    char charBuf[50];
    value.toCharArray(charBuf, 50);
    for (int i = 3; i <= 3; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(4);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }   
  }

void poisoningPrevention(){

  for (int i = 0; i <= 3; i++) {
    digitalWrite(displayAnods[i], HIGH);

      for (int j = 0; j <= 9; j++) {
          digitalWrite(displayDigits[j], HIGH);
          delay(500);
          digitalWrite(displayDigits[j], LOW);
        }
    //digitalWrite(displayAnods[i], LOW);
    }

    for (int i = 3; i >= 0; i--) {
    //digitalWrite(displayAnods[i], LOW);

      for (int j = 9; j >= 0; j--) {
          digitalWrite(displayDigits[j], HIGH);
          delay(500);
          digitalWrite(displayDigits[j], LOW);
        }
    digitalWrite(displayAnods[i], LOW);
    }  
  }








void loop()
{


   digitalWrite(cyfra_7, HIGH);
 // poisoningPrevention();
  
 }
