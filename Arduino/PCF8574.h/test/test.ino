#include <PCF8574.h>
#include <Wire.h>

PCF8574 expander;

int switch_1 = 7;
int switch_2 = 6;
int switch_3 = 5;
int switch_4 = 4;

void setup()
{
  Serial.begin(9600);
  
  expander.begin(0x20);
  expander.pinMode(switch_1, INPUT_PULLUP);
  expander.pinMode(switch_2, INPUT_PULLUP);
  expander.pinMode(switch_3, INPUT_PULLUP);
  expander.pinMode(switch_4, INPUT_PULLUP);
}

void loop()
{

  if (expander.digitalRead(7) == LOW) { //Jeśli przycisk wciśnięty
    Serial.print("wcisniety\n");
    delay(1000);
    } else { 
     Serial.print("zwolniony\n");
     delay(1000);
  }

}
