/*  Keypadtest.pde
 *
 *  Demonstrate the simplest use of the  keypad library.
 *
 *  The first step is to connect your keypad to the
 *  Arduino  using the pin numbers listed below in
 *  rowPins[] and colPins[]. If you want to use different
 *  pins then  you  can  change  the  numbers below to
 *  match your setup.
 *
 */
#include <Wire.h>
#include <Keypad.h>


# define Start_Byte 0x7E
# define Version_Byte 0xFF
# define Command_Length 0x06
# define End_Byte 0xEF
# define Acknowledge 0x00 //Returns info with command 0x41 [0x01: info, 0x00: no info]

# define ACTIVATED LOW


char keyValue1 = '0';
char keyValue2 = '0';
char keyValue3 = '0';

String totalSum = "0000";

String keyMap[][2] = { 
{"1","48"}, 
{"2","14"}, 
{"3","61"}, 
{"4","56"}, 
{"5","36"},
{"6","16"}, 
{"7","57"}, 
{"8","25"}, 
{"9","12"}, //x1
{"a","480"}, 
{"b","140"}, 
{"c","610"}, 
{"d","560"}, 
{"e","360"}, 
{"f","160"},
{"g","570"}, 
{"h","250"}, 
{"i","120"}, //x10
{"j","4800"}, 
{"k","1400"},
{"l","6100"}, 
{"m","5600"}, 
{"n","3600"}, 
{"o","1600"}, 
{"u","5700"},
{"p","2500"},
{"r","1200"}}; //x100



const byte ROWS = 1; // Four rows
const byte COLS = 9; // Three columns
char keys[ROWS][COLS] = {
  {'1','2','3','4','5','6','7','8','9'},  
};
byte rowPins[ROWS] = { 11};
byte colPins[COLS] = { 2, 3, 4, 5, 6, 7, 8, 9, 10 };

Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );


const byte ROWS1 = 1; // Four rows
const byte COLS1 = 9; // Three columns
char keys1[ROWS1][COLS1] = {
  {'a','b','c','d','e','f','g','h','i'},  
};
byte rowPins1[ROWS1] = { 12};
byte colPins1[COLS1] = { 2, 3, 4, 5, 6, 7, 8, 9, 10 };

Keypad kpd1 = Keypad( makeKeymap(keys1), rowPins1, colPins1, ROWS1, COLS1 );

const byte ROWS2 = 1; // Four rows
const byte COLS2 = 9; // Three columns
char keys2[ROWS2][COLS2] = {
  {'j','k','l','m','n','o','u','p','r'},  
};
byte rowPins2[ROWS2] = { 14};
byte colPins2[COLS2] = { 2, 3, 4, 5, 6, 7, 8, 9, 10 };

Keypad kpd2 = Keypad( makeKeymap(keys2), rowPins2, colPins2, ROWS2, COLS2 );


void setup()
{
  Wire.begin(8);                // join i2c bus with address #8
  Wire.onRequest(requestEvent); // register event
  Serial.begin(9600);
}

char* string2char(String command){
    if(command.length()!=0){
        char *p = const_cast<char*>(command.c_str());
        return p;
    }
}

String getValueFromArray(char key){
  String val = "0";  
  for(int i=0; i <= 26; i++){
  char chArray = key;
  String str(chArray);
    if(keyMap[i][0] == str){      
       val = keyMap[i][1];       
    }}
  return val;
}

void keyboardListner(){
  char key = kpd.getKey();  
    if(key)  // Check for a valid key.
    {
      keyValue1 = key;        
    }
    if(!kpd.getState()){
      keyValue1 = '0';
    }
    
  char key1 = kpd1.getKey();  
    if(key1)  // Check for a valid key.
    {
      keyValue2 = key1;    
    }
    if(!kpd1.getState()){
      keyValue2 = '0';
    }

  char key2 = kpd2.getKey();  
    if(key2)  // Check for a valid key.
    {
      keyValue3 = key2;    
    }
    if(!kpd2.getState()){
      keyValue3 = '0';
    }
  }

  String getSum(String val, String val1, String val2){
    String sum = "0000";

    int int_val = val.toInt();
    int int_val1 = val1.toInt();
    int int_val2 = val2.toInt();

    int total = int_val + int_val1 + int_val2;
    sum = String(total);
    return sum;
  }

void loop()
{
  keyboardListner();

  String number_1 = getValueFromArray(keyValue1);
  String number_2 = getValueFromArray(keyValue2);
  String number_3 = getValueFromArray(keyValue3);

  totalSum = getSum(number_1, number_2, number_3);
  playIfFind("5756");;
  //Serial.println(totalSum); 
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {  
  Wire.write(totalSum.c_str()); // respond with message of 6 bytes
  // as expected by master
}



bool flag = 1;
void playIfFind(String number){
  //String str(totalSum);  
  //Serial.println(str.substring(0,4));         // print the character
  
  if((totalSum == number) && flag){
      delay(200);
      if((totalSum == number) && flag){
        //Serial.println("HUUUURAAA");
         playFirst();
         flag = 0;
      }
      
  }

  if (flag == 0 && totalSum != number){
    flag = 1;
  }
}





void playFirst()
{
  execute_CMD(0x3F, 0, 0);
  delay(50);
  setVolume(20);
  delay(50);
  //execute_CMD(0x11,0,1); 
  execute_CMD(0x01,0,1);
  delay(50);
}

void pause()
{
  execute_CMD(0x0E,0,0);
  delay(500);
}

void play()
{
  execute_CMD(0x0D,0,1); 
  delay(500);
}

void playNext()
{
  execute_CMD(0x01,0,1);
  delay(500);
}

void playPrevious()
{
  execute_CMD(0x02,0,1);
  delay(500);
}

void setVolume(int volume)
{
  execute_CMD(0x06, 0, volume); // Set the volume (0x00~0x30)
  delay(2000);
}

void execute_CMD(byte CMD, byte Par1, byte Par2)
// Excecute the command and parameters
{
// Calculate the checksum (2 bytes)
word checksum = -(Version_Byte + Command_Length + CMD + Acknowledge + Par1 + Par2);
// Build the command line
byte Command_line[10] = { Start_Byte, Version_Byte, Command_Length, CMD, Acknowledge,
Par1, Par2, highByte(checksum), lowByte(checksum), End_Byte};
//Send the command line to the module
for (byte k=0; k<10; k++)
{
Serial.write( Command_line[k]);
}
}






