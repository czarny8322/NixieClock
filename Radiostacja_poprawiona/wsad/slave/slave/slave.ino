#include <Wire.h>

#include "SoftwareSerial.h"
//#include <DFPlayer_Mini_Mp3.h>


# define Start_Byte 0x7E
# define Version_Byte 0xFF
# define Command_Length 0x06
# define End_Byte 0xEF
# define Acknowledge 0x00 //Returns info with command 0x41 [0x01: info, 0x00: no info]

# define ACTIVATED LOW




#define lampa_1 13
#define lampa_2 14
#define lampa_3 15
#define lampa_4 16

#define cyfra_9 12
#define cyfra_8 11
#define cyfra_7 10
#define cyfra_6 9
#define cyfra_5 8
#define cyfra_4 7
#define cyfra_3 6
#define cyfra_2 5
#define cyfra_1 4
#define cyfra_0 3

const int buzzer = 2; //buzzer to arduino pin D2
String poisonIndicator = "0001";

char valueFromI2C[4];

int timer = 4;

int displayAnods[] = {
    13 // 1
    ,
    14 // 2
    ,
    15 // 3
    ,
    16 // 4
};

int displayDigits[] = {
    3 // 0
    ,
    4 // 1
    ,
    5 // 2
    ,
    6 // 3
    ,
    7 // 4
    ,
    8 // 5
    ,
    9 // 6
    ,
    10 // 7
    ,
    11 // 8
    ,
    12 // 9
};


// the setup function runs once when you press reset or power the board
void setup()
{
  
    Serial.begin(9600);
    //mySerial.begin (9600);
    //mp3_set_serial (Serial);  //set softwareSerial for DFPlayer-mini mp3 module 
    //mp3_set_volume (10);

    Wire.begin();                // join i2c bus with address #8    
  
    //Serial.println("Initialize DS3231");
    
    // initialize digital pin LED_BUILTIN as an output.
    pinMode(lampa_1, OUTPUT);
    pinMode(lampa_2, OUTPUT);
    pinMode(lampa_3, OUTPUT);
    pinMode(lampa_4, OUTPUT);
    
    pinMode(buzzer, OUTPUT); // Set buzzer - pin D2 as an output

    for (int thisPin = 3; thisPin < 12; thisPin++) {
        pinMode(thisPin, OUTPUT);
    }
}

void anodes_OFF()
{
    digitalWrite(lampa_1, LOW);
    digitalWrite(lampa_2, LOW);
    digitalWrite(lampa_3, LOW);
    digitalWrite(lampa_4, LOW);
}

void anodes_ON()
{
    digitalWrite(lampa_1, HIGH);
    digitalWrite(lampa_2, HIGH);
    digitalWrite(lampa_3, HIGH);
    digitalWrite(lampa_4, HIGH);
}

void displayValue(String value){
    char charBuf[50];
    value.toCharArray(charBuf, 50);
    for (int i = 0; i <= 3; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(4);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }   
  }

void poisoningPrevention(){

  for (int i = 0; i <= 3; i++) {
    digitalWrite(displayAnods[i], HIGH);

      for (int j = 0; j <= 9; j++) {
          digitalWrite(displayDigits[j], HIGH);
          delay(100);
          digitalWrite(displayDigits[j], LOW);
        }
    //digitalWrite(displayAnods[i], LOW);
    }

    for (int i = 3; i >= 0; i--) {
    //digitalWrite(displayAnods[i], LOW);

      for (int j = 9; j >= 0; j--) {
          digitalWrite(displayDigits[j], HIGH);
          delay(100);
          digitalWrite(displayDigits[j], LOW);
        }
    digitalWrite(displayAnods[i], LOW);
    }  
  }

void receiveValueFromArduino(){
  Wire.requestFrom(8, 4);    // request 6 bytes from slave device #8 
  if(Wire.available()){
  for(int i = 0 ; i <=3 ; i++){
        char c = Wire.read(); // receive a byte as character
        valueFromI2C[i] = c;  
      } 
    }
  }

bool flag = 1;

void playIfFind(String number){
  String str(valueFromI2C);  
  Serial.println(str.substring(0,4));         // print the character
  
  if((str.substring(0,4) == number) && flag){
      Serial.begin(9600);
      Serial.println("HUUUURAAA");
      playFirst();
      flag = 0;
  }

  if (flag == 0 && str.substring(0,4) != number){
    flag = 1;
  }
}

void loop()
{  
 receiveValueFromArduino();
 displayValue(valueFromI2C);

 //playIfFind("5756");

 
 //mp3_play (1);


  
 //playFirst();
}


void playFirst()
{
  execute_CMD(0x3F, 0, 0);
  delay(50);
  setVolume(20);
  delay(50);
  //execute_CMD(0x11,0,1); 
  execute_CMD(0x01,0,1);
  delay(50);
}

void pause()
{
  execute_CMD(0x0E,0,0);
  delay(500);
}

void play()
{
  execute_CMD(0x0D,0,1); 
  delay(500);
}

void playNext()
{
  execute_CMD(0x01,0,1);
  delay(500);
}

void playPrevious()
{
  execute_CMD(0x02,0,1);
  delay(500);
}

void setVolume(int volume)
{
  execute_CMD(0x06, 0, volume); // Set the volume (0x00~0x30)
  delay(2000);
}

void execute_CMD(byte CMD, byte Par1, byte Par2)
// Excecute the command and parameters
{
// Calculate the checksum (2 bytes)
word checksum = -(Version_Byte + Command_Length + CMD + Acknowledge + Par1 + Par2);
// Build the command line
byte Command_line[10] = { Start_Byte, Version_Byte, Command_Length, CMD, Acknowledge,
Par1, Par2, highByte(checksum), lowByte(checksum), End_Byte};
//Send the command line to the module
for (byte k=0; k<10; k++)
{
Serial.write( Command_line[k]);
}
}



