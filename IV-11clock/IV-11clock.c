#include <avr/io.h>		// include I/O definitions (port names, pin names, etc)
#include <avr/signal.h>	// include "signal" names (interrupt names)
#include <avr/interrupt.h>	// include interrupt support
#include <avr/wdt.h>
#include <util/delay.h>
#include "global.h"
#include "IV-11clock.h"



/* ********************** 
PORTB 0,1,2 - wyb�r lampy
PORTB 3,4 PORTA 0 - wej�cie joysticka
PORTD 0,1,2.3.4.5.6 - sterowaie segmentami
PORTA 1 - kropka
************************ */

u08 lamps[6] ;
u08 dots[6] = {1,0,1,0,1,0};
u08 t7seg[] = {0b01111101, 0b00100100, 0b01010111, 0b001100111, 0b00101110, 0b01101011, 0b01111011, 0b00100101, 0b01111111, 0b01101111, 0};
u08 setup = 0;
u08 currentlamp = 4;
u08 counter;
u08 dark;


main()
{
DDRD = 0xFF;


DDRB = 0b111;
DDRA = 0b10;
//PORTA = 0b11;
startosc(0);
_delay_ms(2000);

TCCR0B = 0b01;
TIMSK = 0b10;
sei();



while(1)
	{

	if(counter == 60) { counter = 0;gettime();}
	if (bit_is_clear(PINB, 3)) 
		{
	
		if (setup) setup = 0;
		else setup++; 
		_delay_ms(500);
		cli();
	
		sei();
		}

if(setup)
{
u16 tmp = 0;
		if (bit_is_clear(PINB, 4)) 
		{	
			
			dectime(); 
			gettime(); 		
			
			while (tmp != 60000)
			{
				tmp++;
				if (bit_is_set(PINB, 4)) break;
			}
				
	
		
		while (bit_is_clear(PINB, 4)) {  dectime(); gettime(); }}

		if (bit_is_clear(PINA, 0)) 
		{	
			
			inctime(); 
			gettime(); 
		
			while (tmp != 60000)
			{
				tmp++;
				if (bit_is_set(PINA, 0)) break;
			}
	
		
		while (bit_is_clear(PINA, 0)) {  inctime(); gettime(); }}
		}
	}
}





void startosc(u08 mode)
{
u08 to_send[3] = {DS1370_W, 0x7, 0b00010000};
	if(mode)
	to_send[2] = 0b00010000;
	else 
	to_send[2] = 0b00010011;
	USI_TWI_Master_Initialise(  );
	USI_TWI_Start_Transceiver_With_Data( to_send , 3 );
}

u08 BCDto7seg(u08 BCDdigit)
{
return t7seg[BCDdigit];
/*
switch(BCDdigit)
{
 case '!': 
    return 0b0;
  break;
  	
  case 0: 
    return 0b01111101;
  break;

  case 1: 
    return 0b00100100;
  break;

    case 2: 
    return 0b01010111;
  break;

    case 3: 
    return 0b001100111;
  break;

    case 4: 
    return 0b00101110;
  break;

    case 5: 
    return 0b01101011;
  break;

  case 6: 
    return 0b01111011;
	break;
  case 7: 
    return 0b00100101;
	break;

  case 8: 
    return 0b01111111;
	break;

	case 9: 
    return 0b01101111;
  break;
  }*/
}
	
	
ISR(TIMER0_OVF_vect)
{
	PORTB = 5;
	counter++;
	if ((counter%2) && dark) return;
	PORTA &= 0b11111101;
	
	switch(currentlamp)
	{
		case 2:	
		PORTD = BCDto7seg(lamps[0]);
		if (dots[0]) PORTA |= 0b00000010;
		PORTB = currentlamp = 6;
		
		break;
		
		case 6:

		PORTD = BCDto7seg(lamps[1]);
		if (dots[1]) PORTA |= 0b00000010;
		PORTB = currentlamp = 3;
		break;
		
		case 3:

		PORTD = BCDto7seg(lamps[2]);
		if (dots[2]) PORTA |= 0b00000010;
		PORTB = currentlamp = 7;
		break;

		case 7:	
		PORTB = 5;
		PORTD = BCDto7seg(lamps[3]);
		if (dots[3]) PORTA |= 0b00000010;
		PORTB = currentlamp = 0;
		break;

		case 0:

		PORTD = BCDto7seg(lamps[4]);
		if (dots[4]) PORTA |= 0b00000010;
		PORTB = currentlamp = 4;
		break;

		case 4:

		PORTD = BCDto7seg(lamps[5]);
		if (dots[5]) PORTA |= 0b00000010;
		PORTB = currentlamp = 2;
		break;	
	}


} 
