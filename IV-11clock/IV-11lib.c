#include "global.h"
#include "IV-11clock.h"

void gettime()
{
cli();
extern u08 lamps[];
extern u08 dark;
extern u08 setup;
u08 time[4] = {DS1370_R, 0,0,0};
u08 to_send[2] = {DS1370_W, 0x0};
USI_TWI_Master_Initialise();
USI_TWI_Start_Transceiver_With_Data( to_send , 2 );
USI_TWI_Start_Transceiver_With_Data( time , 4 );
u08 seconds;
u08 minutes;
u08 hours;
seconds = time[1];
lamps[0] = time[1]&0b00001111;
seconds = seconds>>4;
lamps[1] = seconds;

minutes = time[2];
lamps[2] = time[2]&0b00001111;
minutes = minutes>>4;
lamps[3] = minutes;

hours = time[3];
lamps[4] = time[3]&0b00001111;
hours = hours>>4;
hours = hours&0b00000011;


u08 tmp;
tmp = hours*10 + lamps[4];
if (tmp>22 || tmp <6) dark = 1;
else dark = 0;

if(setup)
{
	if (lamps[0]%2) dark = 1;
	else dark = 0;
}

if(!hours) hours = '10';
lamps[5] = hours;



sei();
}

void inctime()
{
cli();
 u08 time[4] = {DS1370_R, 0,0,0};
u08 to_send[2] = {DS1370_W, 0x0};
USI_TWI_Master_Initialise();
USI_TWI_Start_Transceiver_With_Data( to_send , 2 );
USI_TWI_Start_Transceiver_With_Data( time , 4 );

(time[2])++;
if((time[2]&0b00001111) > 9)
	{
	time[2] = (time[2]) & 0b11110000;
	time[2] = (time[2]) + 0x10;
	if (time[2] >= 96)
		{
		time[2] = 0;
		(time[3])++;
		if((time[3]&0b00001111) > 9)
			{
			time[3]=time[3]&0b11110000;
			time[3]=time[3]+0b00010000;
			}
			if(time[3] >= 36)
				{
				time[3] = 0;
				}
			
		}
	}

	u08 s_time[5] = {DS1370_W, 0, 0, time[2], time[3]};
USI_TWI_Master_Initialise(  );
	USI_TWI_Start_Transceiver_With_Data( s_time , 5 );
	sei();
}

void dectime()
{
cli();
 u08 time[4] = {DS1370_R, 0,0,0};
u08 to_send[2] = {DS1370_W, 0x0};
USI_TWI_Master_Initialise();
USI_TWI_Start_Transceiver_With_Data( to_send , 2 );
USI_TWI_Start_Transceiver_With_Data( time , 4 );

(time[2])--;
if((time[2]&0b00001111) > 9)
	{
	time[2] = (time[2]) & 0b11110000;
	time[2] = (time[2]) + 0x9;

	if (time[2] >= 96)
		{
		time[2] = 0x59;
		(time[3])--;
		if((time[3]&0b00001111) > 9)
			{
			time[3]=time[3]&0b11110000;
			time[3]=time[3] + 0x9;
			}
			if(time[3] >= 36)
				{
				time[3] = 0x23;
				}
			
		}
	}

	u08 s_time[5] = {DS1370_W, 0, 0, time[2], time[3]};
USI_TWI_Master_Initialise(  );
	USI_TWI_Start_Transceiver_With_Data( s_time , 5 );
	sei();
}

