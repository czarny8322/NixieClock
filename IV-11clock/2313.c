#include <avr/io.h>		// include I/O definitions (port names, pin names, etc)
#include <avr/signal.h>	// include "signal" names (interrupt names)
#include <avr/interrupt.h>	// include interrupt support
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <util/delay.h>

main()
{
	DDRD = 0xFF;
	while(1)
	{
		PORTD = 0;
		_delay_ms(300);
		PORTD = 0xFF;
		_delay_ms(300);
	}
	}
