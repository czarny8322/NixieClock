#include <Wire.h>
#include <DS3231.h>


#include <Timers.h>
Timer dateDisplayTimer;
Timer timerForChangeDisplay;
Timer timerForAlarm;

Timer alarmTimeLong;
bool isAlarmON = false;

bool setUpAlarm = false;

bool dateInterval;
bool alarmInterval;

DS3231 clock;
RTCDateTime dt;
RTCAlarmTime alarm;

#define lampa_1 16
#define lampa_2 15
#define lampa_3 14
#define lampa_4 13
#define sekundnik 17

#define cyfra_9 12
#define cyfra_8 11
#define cyfra_7 10
#define cyfra_6 9
#define cyfra_5 8
#define cyfra_4 7
#define cyfra_3 6
#define cyfra_2 5
#define cyfra_1 4
#define cyfra_0 3

#define button_1 0    // set Time/Temp/PoisonPrev
#define button_2 1    // set alarm
#define button_3 20    // first value change
#define button_4 21    // second value change

const int buzzer = 2; //buzzer to arduino pin D2

// Variables will change:
int index = 1;

int setIndex = 0;
int setTimeView = 0;

int setAlarmView = 0;

int currentView = 0;

bool poisonView = true;

String poisonIndicator = "0001";

int timer = 4;

int displayAnods[] = {
    lampa_1 // 1
    ,
    lampa_2 // 2
    ,
    lampa_3 // 3
    ,
    lampa_4 // 4
};

int displayDigits[] = {
    3 // 0
    ,
    4 // 1
    ,
    5 // 2
    ,
    6 // 3
    ,
    7 // 4
    ,
    8 // 5
    ,
    9 // 6
    ,
    10 // 7
    ,
    11 // 8
    ,
    12 // 9
};

byte year, month, date, DoW, hour, minute, second;

// the setup function runs once when you press reset or power the board
void setup()
{
    Serial.begin(9600);
    Serial.println("Initialize DS3231");

    dateDisplayTimer.begin(SECS(4));
    timerForChangeDisplay.begin(200);
    timerForAlarm.begin(500);

    alarmTimeLong.begin(SECS(30));
   
    clock.begin();

    // initialize digital pin LED_BUILTIN as an output.
    pinMode(lampa_1, OUTPUT);
    pinMode(lampa_2, OUTPUT);
    pinMode(lampa_3, OUTPUT);
    pinMode(lampa_4, OUTPUT);
    pinMode(sekundnik, OUTPUT);
    
    pinMode(buzzer, OUTPUT); // Set buzzer - pin D2 as an output

    for (int thisPin = 3; thisPin < 12; thisPin++) {
        pinMode(thisPin, OUTPUT);
    }

    pinMode(button_1, INPUT);
    pinMode(button_2, INPUT);
   
}

void anodes_OFF()
{
    digitalWrite(lampa_1, LOW);
    digitalWrite(lampa_2, LOW);
    digitalWrite(lampa_3, LOW);
    digitalWrite(lampa_4, LOW);
}

void anodes_ON()
{
    digitalWrite(lampa_1, HIGH);
    digitalWrite(lampa_2, HIGH);
    digitalWrite(lampa_3, HIGH);
    digitalWrite(lampa_4, HIGH);
}

void poisoningPreventionSettings(){
  
   String value = poisonIndicator;
    char charBuf[50];
    value.toCharArray(charBuf, 50);
    for (int i = 3; i <= 3; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(4);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }   
  }

void poisoningPrevention(){

  for (int i = 0; i <= 3; i++) {
    digitalWrite(displayAnods[i], HIGH);

      for (int j = 0; j <= 9; j++) {
          digitalWrite(displayDigits[j], HIGH);
          delay(100);
          digitalWrite(displayDigits[j], LOW);
        }
    //digitalWrite(displayAnods[i], LOW);
    }

    for (int i = 3; i >= 0; i--) {
    //digitalWrite(displayAnods[i], LOW);

      for (int j = 9; j >= 0; j--) {
          digitalWrite(displayDigits[j], HIGH);
          delay(100);
          digitalWrite(displayDigits[j], LOW);
        }
    digitalWrite(displayAnods[i], LOW);
    }  
  }


void displayTime(int timer)
{

    dt = clock.getDateTime();
    String _time = clock.dateFormat("Hi", dt);
    //Serial.println(_time);

    char charBuf[50];
    _time.toCharArray(charBuf, 50);
    for (int i = 0; i <= 3; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(timer);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }

    blink_ON();
}

void displayAlarm(int timer)
{
    alarm = clock.getAlarm2();
    String _alarm = clock.dateFormat("Hi", alarm);

    char charBuf[50];
    _alarm.toCharArray(charBuf, 50);
    for (int i = 0; i <= 3; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(timer);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }

    blink_OFF();
}

void setHourAlarmView(){
  alarm = clock.getAlarm2();
    String _alarm = clock.dateFormat("Hi", alarm);

    char charBuf[50];
    _alarm.toCharArray(charBuf, 50);
    for (int i = 0; i <= 1; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(timer);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }

    blink_OFF();
}

void setMinuteAlarmView(){
  alarm = clock.getAlarm2();
    String _alarm = clock.dateFormat("Hi", alarm);

    char charBuf[50];
    _alarm.toCharArray(charBuf, 50);
    for (int i = 2; i <= 3; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(timer);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }

    blink_OFF();
}


void blink_ON()
{
    int _seconds = dt.second;

    if (_seconds % 2 == 0) {
        digitalWrite(sekundnik, HIGH);
    }
    else {
        digitalWrite(sekundnik, LOW);
    }
}

void blink_OFF()
{
  digitalWrite(sekundnik, LOW);
}

void displayTemp(int timer)
{
    String _temp = String(int(round(clock.readTemperature())));
    char charBuf[50];
    _temp.toCharArray(charBuf, 50);
    for (int i = 0; i <= 1; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(4);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }
}

void displayDate(int timer)
{

    dt = clock.getDateTime();
    String _date;

    if (dateDisplayTimer.available()) {
        dateInterval ^= true;
        dateDisplayTimer.restart();
    }

    if (dateInterval) {
        _date = clock.dateFormat("dm", dt);
    }
    else {
        _date = clock.dateFormat("Y", dt);
    }

    char charBuf[50];
    _date.toCharArray(charBuf, 50);
    for (int i = 0; i <= 3; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(4);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }
}

void changeView()
{
    index++;
    index = index % 4; // 0 1 2 3
    
    delay(200);
    currentView = index;
}

void changeSetTimeView(int views){
    setIndex++;
    setIndex = setIndex % views; //0 1 2    
    delay(200);
    setTimeView = setIndex;
}

void changeMinuts()
{

    if (timerForChangeDisplay.available()) {
        minute = dt.minute;
        minute++;
        if (minute == 60) {
            minute = 0;
        }
        clock.setDateTime(dt.year, dt.month, dt.day, dt.hour, minute, 00);
        timerForChangeDisplay.restart();
    }
}

void changeHours()
{

    if (timerForChangeDisplay.available()) {
        hour = dt.hour;
        hour++;
        if (hour == 24) {
            hour = 0;
        }
        clock.setDateTime(dt.year, dt.month, dt.day, hour, dt.minute, 00);

        timerForChangeDisplay.restart();
    }
}

void changeAlarmHours(){
  
  if (timerForChangeDisplay.available()) {
    
        hour = alarm.hour;
        hour++;
        if (hour == 24) {
            hour = 0;
        }        
        clock.setAlarm2(0, hour, alarm.minute,     DS3231_MATCH_M);
        timerForChangeDisplay.restart();
    }
}

void changeAlarmMinuts(){
  
  if (timerForChangeDisplay.available()) {
    
       minute = alarm.minute;
        minute++;
        if (minute == 60) {
            minute = 0;
        }        
        clock.setAlarm2(0, alarm.hour, minute,     DS3231_MATCH_M);
        timerForChangeDisplay.restart();
    }
}

void runAlarm(){
  
  if (timerForAlarm.available()) {
        alarmInterval ^= true;
        timerForAlarm.restart();
    }
   if (alarmInterval) {
      tone(buzzer, 1500); // Send 1KHz sound signal...

            Serial.println("ALARM ON");
           }
    else {
      noTone(buzzer);     // Stop sound...
      Serial.println("ALARM OFF");
    }
      
  }

  void setHourView(){    
        dt = clock.getDateTime();
    String _time = clock.dateFormat("Hi", dt);
    //Serial.println(_time);

    char charBuf[50];
    _time.toCharArray(charBuf, 50);
    for (int i = 0; i <= 1; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(timer);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }
    blink_ON();    
  }


void setMinuteView(){    
        dt = clock.getDateTime();
    String _time = clock.dateFormat("Hi", dt);
    //Serial.println(_time);

    char charBuf[50];
    _time.toCharArray(charBuf, 50);
    for (int i = 2; i <= 3; i++) {
        int numberInt = charBuf[i] - '0';
        digitalWrite(displayAnods[i], HIGH);
        digitalWrite(displayDigits[numberInt], HIGH);
        delay(timer);
        digitalWrite(displayDigits[numberInt], LOW);
        digitalWrite(displayAnods[i], LOW);
    }

    blink_ON();
    
  }
  
void loop()
{  

    switch (currentView) {
    case 0: 
          if (digitalRead(button_1) == LOW) {    
              changeSetTimeView(3);    
              Serial.println(setTimeView);
              delay(200);
          }
            switch (setTimeView){
              case 0:
                displayTime(timer);
              break;
              case 1:
                setHourView();
                 if (digitalRead(button_2) == LOW & setTimeView == 1) {    
                    changeHours();
                    } 
              break;
              case 2:
                setMinuteView(); 
                     if (digitalRead(button_2) == LOW & setTimeView == 2) {    
                    changeMinuts();
                    }          
              break;
            }
        break;
        
    case 1:    
    if (digitalRead(button_1) == LOW) {    
                  changeSetTimeView(4);
                  Serial.println(setAlarmView);
                  delay(200);
              }
                switch (setTimeView){
                  case 0:
                    displayAlarm(timer);
                  break;
                  case 1:
                    setHourAlarmView();
                     if (digitalRead(button_2) == LOW & setTimeView == 1) {    
                        changeAlarmHours();
                        } 
                  break;
                  case 2:
                    setMinuteAlarmView(); 
                         if (digitalRead(button_2) == LOW & setTimeView == 2) {    
                        changeAlarmMinuts();
                        }          
                  break;
                   case 3:
                    if (digitalRead(button_2) == LOW) {
                            setUpAlarm ^= true;
                            tone(buzzer, 1500);
                            delay(200);
                            noTone(buzzer);             
                        }
                
                       if(setUpAlarm){
                          digitalWrite(sekundnik, HIGH);
                                   }else{
                            digitalWrite(sekundnik, LOW);
                         }      
                  break;
                }

                if(setUpAlarm){
                          digitalWrite(sekundnik, HIGH);
                                   }else{
                            digitalWrite(sekundnik, LOW);
                         }   

        break;  
    case 2:
        poisoningPreventionSettings();                
        if (digitalRead(button_1) == LOW) {            
           poisonView = !poisonView;
           delay(200);
        }        
          if(poisonView){        
                 poisonIndicator = "0000";
               } else{                
                  poisonIndicator = "0001";
                }             
        blink_OFF();        
        break;
        
    case 3:
       displayTemp(timer);
        blink_OFF();
        break; 
}




       
    if (digitalRead(button_2) == LOW & setTimeView == 0) {
      changeView();  
      setIndex = 0;
      setTimeView = 0;      
    }        


if(poisonIndicator == "0001"){
    if((dt.minute == dt.hour) && (dt.hour == dt.second)){
      poisoningPrevention();
      currentView = 0;
    } 
    }
    
  // Sprawdzamy alaram 1
  if (clock.isAlarm2() & setUpAlarm)
  { 
    Serial.println("ALARM 2 TRIGGERED!");
    alarmTimeLong.restart();     
    isAlarmON = true;
   }
  
  if(isAlarmON){
    currentView = 0; //set view on time
    
    runAlarm();     
    if(alarmTimeLong.available()){
      isAlarmON = false;
      noTone(buzzer);
      Serial.println("ALARM STOP");
      
    }
      
      }

if (digitalRead(button_1) == LOW & isAlarmON) {
       noTone(buzzer);
       isAlarmON = false;
       delay(200);
    }

      
  }    

