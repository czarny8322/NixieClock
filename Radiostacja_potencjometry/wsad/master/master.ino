/*  Keypadtest.pde
 *
 *  Demonstrate the simplest use of the  keypad library.
 *
 *  The first step is to connect your keypad to the
 *  Arduino  using the pin numbers listed below in
 *  rowPins[] and colPins[]. If you want to use different
 *  pins then  you  can  change  the  numbers below to
 *  match your setup.
 *
 */
#include <Wire.h>

String MY_NUMBER = "4712";

#define POT1 A0
#define POT2 A1
#define POT3 A2
#define POT4 A3

#define STER1 7
#define STER2 6
#define STER3 5
#define STER4 4
#define STER5 3
#define STER6 2

# define Start_Byte 0x7E
# define Version_Byte 0xFF
# define Command_Length 0x06
# define End_Byte 0xEF
# define Acknowledge 0x00 //Returns info with command 0x41 [0x01: info, 0x00: no info]

# define ACTIVATED LOW

String totalSum = "0000";

void setup()
{
  Wire.begin(8);                // join i2c bus with address #8
  Wire.onRequest(requestEvent); // register event
  Serial.begin(9600);

  pinMode(STER1, OUTPUT);
  pinMode(STER2, OUTPUT);
  pinMode(STER3, OUTPUT);
  pinMode(STER4, OUTPUT);
  pinMode(STER5, OUTPUT);
  pinMode(STER6, OUTPUT);

  digitalWrite(STER1, LOW);
  digitalWrite(STER2, LOW);
  digitalWrite(STER3, LOW);
  digitalWrite(STER4, LOW);
  digitalWrite(STER5, LOW);
  digitalWrite(STER6, LOW);
}

void loop()
{    
  int POT1_val = map(analogRead(POT1), 0, 1000, 0, 9);
  int POT2_val = map(analogRead(POT2), 0, 1000, 0, 9);
  int POT3_val = map(analogRead(POT3), 0, 1000, 0, 9);
  int POT4_val = map(analogRead(POT4), 0, 1000, 0, 9);

Serial.print("Zakres: ");
Serial.print(analogRead(POT4));
Serial.println(""); 


  String POT1_str = String(POT1_val);
  String POT2_str = String(POT2_val);
  String POT3_str = String(POT3_val);
  String POT4_str = String(POT4_val);

  totalSum = POT1_str + POT2_str + POT3_str + POT4_str;  
  Serial.println(totalSum); 
  
  delay(20);
  
  if(totalSum == MY_NUMBER){
    delay(50);
    digitalWrite(STER1, HIGH);
    delay(20);
  }else digitalWrite(STER1, LOW);
  
  playIfFind(MY_NUMBER);

  
  
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {  
  Wire.write(totalSum.c_str()); // respond with message of 6 bytes
  // as expected by master
}

bool flag = 1;
void playIfFind(String number){
  //String str(totalSum);  
  //Serial.println(str.substring(0,4));         // print the character
  
  if((totalSum == number) && flag){
      delay(200);
      if((totalSum == number) && flag){
        //Serial.println("HUUUURAAA");
         playFirst();
         flag = 0;
      }
      
  }

  if (flag == 0 && totalSum != number){
    flag = 1;
  }
}



void playFirst()
{
  execute_CMD(0x3F, 0, 0);
  delay(50);
  setVolume(20);
  delay(50);
  //execute_CMD(0x11,0,1); 
  execute_CMD(0x01,0,1);
  delay(50);
}

void pause()
{
  execute_CMD(0x0E,0,0);
  delay(500);
}

void play()
{
  execute_CMD(0x0D,0,1); 
  delay(500);
}

void playNext()
{
  execute_CMD(0x01,0,1);
  delay(500);
}

void playPrevious()
{
  execute_CMD(0x02,0,1);
  delay(500);
}

void setVolume(int volume)
{
  execute_CMD(0x06, 0, volume); // Set the volume (0x00~0x30)
  delay(2000);
}

void execute_CMD(byte CMD, byte Par1, byte Par2)
// Excecute the command and parameters
{
// Calculate the checksum (2 bytes)
word checksum = -(Version_Byte + Command_Length + CMD + Acknowledge + Par1 + Par2);
// Build the command line
byte Command_line[10] = { Start_Byte, Version_Byte, Command_Length, CMD, Acknowledge,
Par1, Par2, highByte(checksum), lowByte(checksum), End_Byte};
//Send the command line to the module
for (byte k=0; k<10; k++)
{
Serial.write(Command_line[k]);
}
}






