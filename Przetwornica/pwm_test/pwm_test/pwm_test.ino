#include <TimerOne.h>
//UNO only


//mode 15 fast pwm to OCR1A top
//output on B pin 10
void setup()
{

  pinMode (10, OUTPUT);
  //normal mode set at bottom ,clear on compare match
  //set bits wgm11 and wgm10 part of mode 15
  TCCR1A = 0xA3; //10100011
  TCCR1B = 0x19 ; //00011001 2 bits of mode 15, clock prescale by 1   // (1 << WGM12)|(1 << WGM13)|(1 << CS10);
  
  
  //ICR1 = 127;
  OCR1A = 185;//top value 125khz   //PWM_fequency = clock_speed / [Prescaller_value * (1 + TOP_Value) ]
  TCCR0A = 0x00; //Normal port operation, OC0x disconnected.
  TCCR0B = 0x00; //No clock source (Timer/Counter stopped)
  TCNT1 = 0 ;
  OCR1B = 80;
}
void loop()
{
  

  
}



//PWM_fequency = clock_speed / [Prescaller_value * (1 + TOP_Value) ]
