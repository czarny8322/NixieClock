int hvTargetVoltage = 180;
  int pwmTop = 1000;
  int pulseWidth = 200;

  /* disable global interrupts while we set up them up */
  cli();

  // **************************** HV generator ****************************

  TCCR1A = 0;    // disable all PWM on Timer1 whilst we set it up
  TCCR1B = 0;    // disable all PWM on Timer1 whilst we set it up
  ICR1 = pwmTop; // Our starting point for the period  

  // Configure timer 1 for Fast PWM mode via ICR1, with prescaling=1
  TCCR1A = (1 << WGM11);
  TCCR1B = (1 << WGM13) | (1<<WGM12) | (1 << CS10);

  tccrOff = TCCR1A;
  
  TCCR1A |= (1 <<  COM1A1);  // enable PWM on port PD4 in non-inverted compare mode 2
 
  tccrOn = TCCR1A;
  
  OCR1A = pulseWidth;  // Pulse width of the on time

  /* enable global interrupts */
  sei();
