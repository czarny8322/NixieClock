/*  Keypadtest.pde
 *
 *  Demonstrate the simplest use of the  keypad library.
 *
 *  The first step is to connect your keypad to the
 *  Arduino  using the pin numbers listed below in
 *  rowPins[] and colPins[]. If you want to use different
 *  pins then  you  can  change  the  numbers below to
 *  match your setup.
 *
 */
 #include <Wire.h>
#include <Keypad.h>

const byte ROWS = 1; // Four rows
const byte COLS = 9; // Three columns
char keys[ROWS][COLS] = {
  {'1','2','3','4','5','6','7','8','9'},
  
};
byte rowPins[ROWS] = { 11};
byte colPins[COLS] = { 2, 3, 4, 5, 6, 7, 8, 9, 10 };

Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );


const byte ROWS1 = 1; // Four rows
const byte COLS1 = 9; // Three columns
char keys1[ROWS1][COLS1] = {
  {'a','b','c','d','e','f','g','h','i'},
  
};
byte rowPins1[ROWS1] = { 12};
byte colPins1[COLS1] = { 2, 3, 4, 5, 6, 7, 8, 9, 10 };

Keypad kpd1 = Keypad( makeKeymap(keys1), rowPins1, colPins1, ROWS1, COLS1 );

const byte ROWS2 = 1; // Four rows
const byte COLS2 = 9; // Three columns
char keys2[ROWS2][COLS2] = {
  {'j','k','l','m','n','o','u','p','r'},
  
};
byte rowPins2[ROWS2] = { 13};
byte colPins2[COLS2] = { 2, 3, 4, 5, 6, 7, 8, 9, 10 };

Keypad kpd2 = Keypad( makeKeymap(keys2), rowPins2, colPins2, ROWS2, COLS2 );





#define ledpin 13

void setup()
{
  Wire.begin(8);                // join i2c bus with address #8
  Wire.onRequest(requestEvent); // register event
  Serial.begin(9600);
}

void loop()
{
  char key = kpd.getKey();
  if(key)  // Check for a valid key.
  {
    switch (key)
    {
      case '*':
        digitalWrite(ledpin, LOW);
        break;
      case '#':
        digitalWrite(ledpin, HIGH);
        break;
      default:

        Serial.println(key);
    }
  }


char key1 = kpd1.getKey();
  if(key1)  // Check for a valid key.
  {
    switch (key1)
    {
      case '*':
        digitalWrite(ledpin, LOW);
        break;
      case '#':
        digitalWrite(ledpin, HIGH);
        break;
      default:

        Serial.println(key1);
    }
  }


  char key2 = kpd2.getKey();
  if(key2)  // Check for a valid key.
  {
    switch (key2)
    {
      case '*':
        digitalWrite(ledpin, LOW);
        break;
      case '#':
        digitalWrite(ledpin, HIGH);
        break;
      default:

        Serial.println(key2);
    }
  }


  
  
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
  Wire.write("8456"); // respond with message of 6 bytes
  // as expected by master
}
